/**
 * Created by tavz-correia on 12/5/16.
 */
public class Vendedor extends Empregado {
    private double valorVendas;
    private double comissao;

    public Vendedor(String nome, String endereco, String telefone){
        super(nome, endereco, telefone);
    }

    public double getValorVendas() {
        return valorVendas;
    }

    public void setValorVendas(double valorVendas) {
        this.valorVendas = valorVendas;
    }

    public double getComissao() {
        return comissao;
    }

    public void setComissao(double comissao) {
        this.comissao = comissao;
    }

    @Override
    public double calcularSalario(){
        double comissao = valorVendas / 50;
        setComissao(comissao);
        double salario = getSalarioBase() - (getSalarioBase() * getImposto()) + comissao;
        return salario;
    }

    @Override
    public void imprimePessoa(){
        System.out.println("Nome: " + getNome() + "\n" +
                "Endereco: " + getEndereco() + "\n" +
                "Telefone: " + getTelefone() + "\n" +
                "Codigo do setor: " + getCodigoSetor() + "\n" +
                "Salario Base: " + getSalarioBase() + "\n" +
                "Imposto: " + getImposto() + "\n" +
                "Valor de VEndas: " + getValorVendas()   + "\n" +
                "Comissao: " + getComissao());
    }
}
