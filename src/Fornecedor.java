/**
 * Created by tavz-correia on 12/5/16.
 */
public class Fornecedor extends Pessoa {
    private double valorCredito;
    private double valorDivida;

    public Fornecedor(String nome, String endereco, String telefone){
        super(nome, endereco, telefone);
    }

    public double obterSaldo(){
        return valorCredito - valorDivida;
    }

    public void setValorCredito(double valorCredito){
        this.valorCredito = valorCredito;
    }

    public double getValorCredito() {
        return valorCredito;
    }

    public void setValorDivida(double valorDivida){
        this.valorDivida = valorDivida;
    }

    public double getValorDivida(){
        return valorDivida;
    }

    @Override
    public void imprimePessoa(){
        System.out.println("Nome: " + getNome() + "\n" +
                "Endereco: " + getEndereco() + "\n" +
                "Telefone: " + getTelefone() + "\n" +
                "Valor Creditado: " + getValorCredito() + "\n" +
                "Valor da divida: " + getValorDivida());
    }
}
